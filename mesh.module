<?php

/**
 * Implementation of hook_menu().
 */
function mesh_menu() {
  $items = array();

  $items['admin/content/taxonomy/mesh'] = array(
    'title' => 'MeSH',
    'description' => 'Administer settings for the MeSH taxonomy.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mesh_admin'),
    'access arguments' => array('administer mesh'),
    'file' => 'mesh.admin.inc',
  );
  
  $items['mesh/browser/%ctools_js'] = array(
    'title' => 'MeSH Browser',
    'description' => 'Dynamic callback for browsing MeSH vocabulary.',
    'page callback' => 'mesh_browser',
    'page arguments' => array(2),
    'access arguments' => array('access content'),
    'file' => 'mesh.pages.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implementation of hook_perm().
 */
function mesh_perm() {
  return array('administer mesh');
}

/**
 * Implementation of hook_theme().
 */
function mesh_theme() {
  return array(
    'mesh_browser' => array(
      'arguments' => array('element' => NULL),
    ),
    'mesh_hidden_value' => array(
      'arguments' => array('term' => NULL),
    ),
    'mesh_term' => array(
      'arguments' => array('term' => NULL),
    ),
  );
}

/**
 * Implementation of hook_form_alter().
 *
 * Transform typical taxonomy dropdown into AJAX browser only for MeSH vocabulary.
 */
function mesh_form_alter(&$form, $form_state, $form_id) {
  if ($form['taxonomy'][variable_get('mesh_vocabulary_vid', 0)]) {
    drupal_add_css(drupal_get_path('module', 'mesh') . '/mesh.css');
    drupal_add_js(drupal_get_path('module', 'mesh') . '/mesh.js');

    // Remove preset taxonomy terms
    unset($form['taxonomy'][variable_get('mesh_vocabulary_vid', 0)]['#options']);

    // Load the modal library and add the modal javascript.
    ctools_include('modal');
    ctools_modal_add_js();

    // Generate initial list of terms associated with the node.
    $mesh_list = '';
    if ($form['#node'] && count($form['#node']->taxonomy)) {
      foreach ($form['#node']->taxonomy as $tid => $term) {
        if ($term->vid == variable_get('mesh_vocabulary_vid', 0)) {
          $mesh_list .= theme('mesh_term', $term);
          $mesh_hidden .= theme('mesh_hidden_value', $term);
        }
      }
    }

    unset($form['taxonomy'][variable_get('mesh_vocabulary_vid', 0)]);
    $form['taxonomy'][variable_get('mesh_vocabulary_vid', 0)] = array(
      '#type' => 'hidden',
    );
    $form['taxonomy']['mesh-browser'] = array(
      '#title' => 'MeSH Vocabulary',
      '#suffix' => '<div id="mesh-vocab-list">' . $mesh_list . '</div>',
      '#value' => l('Insert new MeSH term', 'mesh/browser/nojs', array('attributes' => array(
        'class' => 'ctools-use-modal',
      ))),
      '#theme' => 'mesh_browser',
    );
  }
}

/**
 * Theme the MeSH browser.
 */
function theme_mesh_browser($element) {
  return theme('form_element', $element, $element['#value']);
}

/**
 * Render a MeSH taxonomy term's hidden value.
 *
 * @param $term
 *   The term.
 */
function theme_mesh_hidden_value($term) {
  $hidden_term = array(
    '#name' => _mesh_get_field_id($term),
    '#id' => _mesh_get_field_id($term),
    '#value' => $term->tid,
  );
  return theme('hidden', $hidden_term);
}

/**
 * Render a MeSH taxonomy term.
 *
 * @param $term
 *   The term.
 */
function theme_mesh_term($term) {
  return '<div class="mesh-term" id="' . $field_id . '-term">' . theme('mesh_hidden_value', $term) . $term->name . (!empty($term->description) ? ' (' . $term->description . ')' : '') . ' <span class="mesh-term-remove">' . theme('image', 'misc/watchdog-error.png', 'Remove term') . '</span></div>';
}

/**
 * Validate file as being a proper MeSH tree file.
 *
 * @see http://www.nlm.nih.gov/mesh/mtr_abt.html
 * @param $file
 *   A Drupal file object.
 * @return
 *   An array. If the file isn't correctly formatted, it will contain an error message.
 */
function _mesh_validate_trees_files($file) {
  $errors = array();

  $mesh_files = file($file->filepath);

  foreach ($mesh_files as $line) {
    if (!($line = trim($line))) continue;

    if (preg_match('/[[:print:]]+;[\w+\.?]+/', $line) == 0) {
      $errors[] = t('Invalid MeSH tree files syntax. Please follow the <a href="@format_rule">MeSH syntax.</a>', array('@format_rule' => 'http://www.nlm.nih.gov/mesh/mtr_abt.html'));
      break;
    }
  }

  return $errors;
}

/**
 * Delete the MeSH tree file.
 */
function _mesh_delete_file() {
  $path = variable_get('mesh_filepath', '');

  if (!empty($path)) {
    $db_result = db_query('DELETE FROM {files} WHERE filepath = "%s"', array($path));
    $file_result = file_delete($path);

    if (db_affected_rows() && $file_result) {
      variable_set('mesh_filepath', '');
      drupal_set_message(t('MeSH file successfully deleted.'));
    }
    else {
      drupal_set_message(t('Unable to delete MeSH file.'), 'error');
    }
  }
  else {
    drupal_set_message(t('No MeSH file currently exists.'), 'error');
  }
}

/**
 * Start batch process to import MeSH database
 */
function _mesh_populate_vocabulary() {
  $mesh = file(variable_get('mesh_filepath', ''));

  $batch = array(
    'operations' => array(
      array('_mesh_import_terms', array($mesh)),
      ),
    'title' => t('Importing MeSH terms'),
    'finished' => '_mesh_populate_finished',
    'init_message' => t('MeSH import is starting.'),
    'error_message' => t('MeSH has encountered an error.'),
    'file' => drupal_get_path('module', 'mesh') . '/mesh.inc',
  );

  batch_set($batch);
}

/**
 * Return a string representing the term's bracketed field ID.
 * 
 * @param $term
 *   The term.
 */
function _mesh_get_field_id($term) {
  return 'taxonomy[' . $term->vid . '][' . $term->tid . ']';
}

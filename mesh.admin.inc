<?php

/**
 * Configuration form for the MeSH module.
 */
function mesh_admin() {
  module_load_include('inc', 'mesh', 'includes/mesh.trees');

  $form = array();

  $form['#attributes'] = array('enctype' => "multipart/form-data");

  $form['#prefix'] = t('Visit the MeSH vocabulary\'s configuration page at !url.', array('!url' => l('admin/content/taxonomy/edit/vocabulary/' . variable_get('mesh_vocabulary_vid', ''), 'admin/content/taxonomy/edit/vocabulary/' . variable_get('mesh_vocabulary_vid', ''))));

  $form['mesh_year'] = array(
    '#type' => 'radios',
    '#title' => 'MeSH Year',
    '#description' => t('Select the year of the MeSH tree file you are using.'),
    '#options' => _mesh_tree_structure(),
    '#default_value' => variable_get('mesh_year', 0),
  );

  $form['mesh_file'] = array(
    '#type' => 'fieldset',
    '#title' => t('MeSH file'),
    '#description' => t('The current MeSH trees file is located on the <a href="@nlm_url">NIH National Library of Medicine</a> web site.', array('@nlm_url' => 'http://www.nlm.nih.gov/mesh/')),
    '#collapsible' => FALSE,
  );

  // if filepath is set, file already uploaded
  if (variable_get('mesh_filepath', '')) {
    $form['mesh_file']['#description'] .= '<br><br>' . t('The current MeSH file is located at <a href="@mesh_file">@mesh_file.</a>', array('@mesh_file' => base_path() . variable_get('mesh_filepath', '')));
    $form['mesh_file']['delete_mesh_file'] = array(
      '#type' => 'submit',
      '#value' => t('Delete uploaded file'),
      '#submit' => array('_mesh_delete_file'),
    );

    $form['mesh_file']['mesh_populate'] = array(
      '#type' => 'submit',
      '#value' => t('Populate MeSH vocabulary'),
      '#submit' => array('_mesh_populate_vocabulary'),
    );
  }
  else {
    $form['mesh_file']['mesh_file_upload'] = array(
      '#type' => 'file',
      '#title' => t('MeSH file'),
      '#description' => t('Upload the MeSH trees file.'),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Custom submit handler for MeSH admin page.
 */
function mesh_admin_submit($form, &$form_state) {
  variable_set('mesh_year', $form_state['values']['mesh_year']);
  drupal_set_message('MeSH year successfully saved.');

  if (!variable_get('mesh_filepath', '')) {
    $file = file_save_upload('mesh_file_upload', array('_mesh_validate_trees_files' => array()), file_directory_path());

    if ($file != 0) {
      variable_set('mesh_filepath', $file->filepath);
    }
    else {
      form_set_error('mesh_file_upload', t("Failed to save the file."));
    }
  }
}
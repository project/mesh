<?php


/**
 * Provide top-level tree structure not provided by default trees file.
 *
 * @param $year
 *   Specific year to list top-level structure.
 * @param $letter
 *   Individual letter heading.
 * @return
 *   If no arguments provided, an array of years available. If only $year provided,
 *   an array of all letter headings. If $year and $letter provided, a single heading.
 */
function _mesh_tree_structure($year = '', $letter = '') {
  switch ($year) {
    case t('2011'):
      $structure = array(
        'A' => 'Anatomy',
        'B' => 'Organisms',
        'C' => 'Diseases',
        'D' => 'Chemicals and Drugs',
        'E' => 'Analytical,Diagnostic and Therapeutic Techniques and Equipment',
        'F' => 'Psychiatry and Psychology',
        'G' => 'Biological Sciences',
        'H' => 'Natural Sciences',
        'I' => 'Anthropology,Education,Sociology and Social Phenomena',
        'J' => 'Technology,Industry,Agriculture',
        'K' => 'Humanities',
        'L' => 'Information Science',
        'M' => 'Named Groups',
        'N' => 'Health Care',
        'V' => 'Publication Characteristics',
        'Z' => 'Geographicals',
      );

      return empty($letter) ? $structure : $structure[$letter];
    default:
      return array(
        2011 => 2011,
      );
  }
}

function _mesh_import_terms(&$mesh, &$context) {
  module_load_include('inc', 'mesh', 'includes/mesh.trees');

  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['hierarchy'] = array();
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['total'] = count($mesh);
    $context['results']['terms_saved'] = 0;
  }

  $end = 100;
  for ($i = 0; $i < $end && $context['sandbox']['progress'] < $context['sandbox']['total']; ++$i) {
    list($term_name, $term_id) = explode(';', $mesh[$context['sandbox']['progress']++]);
    $depth = count(explode('.', $term_id)) + 1;
    $prev_depth = count($context['sandbox']['hierarchy']);
    
    while ($depth <= $prev_depth--) {
      array_pop($context['sandbox']['hierarchy']);
    }

    if ($depth == 2 && preg_match('/^[A-Z]01$/', $term_id)) {
      $key = substr($term_id, 0, 1);
      $term = array(
        'vid' => variable_get('mesh_vocabulary_vid', 0),
        'name' => _mesh_tree_structure(variable_get('mesh_year', 0), $key),
      );
    
      taxonomy_save_term($term);
      $context['sandbox']['hierarchy'] = array($term['tid']);
    }

    $term = array(
      'vid' => variable_get('mesh_vocabulary_vid', 0),
      'name' => $term_name,
      'description' => 'MeSH ID: ' . trim($term_id),
    );

    $term['parent'] = end($context['sandbox']['hierarchy']);

    taxonomy_save_term($term);
    array_push($context['sandbox']['hierarchy'], $term['tid']);
  }

  $context['results']['terms_saved'] = $context['sandbox']['progress'];

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['total'];
    $context['message'] = t('Processed %current out of %total terms.', array('%current' => $context['sandbox']['progress'], '%total' => $context['sandbox']['total']));
  }
}

function _mesh_populate_finished($success, $results, $operations) {
  if ($success) {
    // Here we do something meaningful with the results.
    $message = t('Import results: @saved terms saved.', array(
        '@saved' => $results['terms_saved'],
    ));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $message = t('An error occurred while processing the MeSH vocabulary.');
  }

  drupal_set_message($message);
}
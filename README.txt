
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer: Erik Webb <erik@erikwebb.net>

The MeSH vocabulary is a commonly-used set of medical terms used to categorize
content. This module simplifies the specific integration of with this 
vocabulary into Drupal's taxonomy system. In addition, the MeSH taxonomy is 
not fully populated by default. Instead, the taxonomy is created dynamically as
needed.

INSTALLATION
------------

The MeSH module requires the latest tree-formatted version of the MeSH 
vocabulary. After this is installed, the vocabulary must be primed for quicker
access.

1. Download MeSH in tree format at http://www.nlm.nih.gov/mesh/filelist.html 
   (registration required).

2. Prime the MeSH cache at admin/content/taxonomy/mesh.

3. Add the MeSH vocabulary to the desired content types at 
   admin/content/taxonomy.

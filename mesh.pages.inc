<?php

/**
 * CTools-based browser of taxonomy
 * 
 * @param $js
 *   A boolean provided by CTools to determine whether client has JavaScript enabled.
 * @param $tid
 *   A taxonomy term ID to use as the parent of the current tree.
 * @param $done
 *   A boolean used to determine if user has selected a specific term.
 * @return
 *   Formatted HTML.
 */
function mesh_browser($js = FALSE, $tid = 0, $done = FALSE) {
  $output = '';
  
  $term = taxonomy_get_term($tid);
      
  if (!$done) {
    if ($tid != 0) {
      $output .= l('Use ' . $term->name . (!empty($term->description) ? ' (' . $term->description . ')' : '') . '.', 'mesh/browser/nojs/' . $tid . '/true', array('attributes' => array(
        'class' => 'ctools-use-ajax message',
      )));
    }
  
    $result = db_query("SELECT td.tid, td.name, td.description FROM {term_data} td
      JOIN {term_hierarchy} th ON td.tid = th.tid
      WHERE th.parent = %d AND td.vid = %d
      ", $tid, variable_get('mesh_vocabulary_vid', 0)
    );

    if (db_affected_rows() > 0) {
      $terms = array();
      while ($row = db_fetch_object($result)) {
        $terms[] = l($row->name . (!empty($row->description) ? ' (' . $row->description . ')' : ''), 'mesh/browser/nojs/' . $row->tid, array('attributes' => array('class' => 'ctools-use-modal',)));
      }

      $output .= theme('item_list', $terms);
    }

    $parents = taxonomy_get_parents($term->tid);
    if ($tid != 0) {
      $parent = count($parents) ? reset($parents) : '';
      $output .= '<br>' . l('[Go up one level.]', 'mesh/browser/nojs/' . (is_object($parent) ? $parent->tid : ''), array('attributes' => array('class' => 'ctools-use-ajax message',)));
    }
  }
  else {
    $list_output = theme('mesh_term', $term);
  }

  if ($js) {
    ctools_include('ajax');
    ctools_include('modal');

    if ($done) {
      ctools_add_js('ajax-responder');

      $commands = array();
      $commands[] = ctools_ajax_command_append('#mesh-vocab-list', $list_output);
      $commands[] = ctools_modal_command_dismiss();
      ctools_ajax_render($commands);
    }
    else {
      ctools_modal_render('MeSH Browser', $output); 
    }
  }
  else {
    return $output;
  }
}
